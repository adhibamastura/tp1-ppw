from news.models import News
from home.models import Report
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.db.models import Q

def index(request):
    search_query = request.GET.get('search', None)
    if(request.user.is_authenticated):
        request.session['full_name'] = "{} {}".format(request.user.first_name, request.user.last_name)
        request.session['username'] = request.user.username

    if(search_query):
        news = News.objects.filter(Q(title__contains=search_query) | Q(desc__contains=search_query))
        reports = Report.objects.filter(Q(title__contains=search_query) | Q(desc__contains=search_query))
        return render(request, 'search.html', {"news" : news, "reports" : reports})

    news = News.objects.all()
    reports = Report.objects.all()
    return render(request, 'index.html', {"news" : news, "reports" : reports})
