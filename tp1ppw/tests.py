from django.test import TestCase
from account.models import Profile
from django.urls import resolve
from django.http import HttpRequest
from news.models import News
from home.models import Report
from django.test import Client
from django.contrib.auth import get_user_model

# Create your tests here.


class TestDonationViewsTemplateTest(TestCase):
    def test_url_donation_is_exist(self):
        response = Client().get('/donation/')
        self.assertEqual(response.status_code,200)

    def test_url_home_is_exist(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code,200)

    def test_url_news_is_exist(self):
        response = Client().get('/news/')
        self.assertEqual(response.status_code,200)

    def test_url_index_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_search_query(self):
        response = Client().get('/?search=query')
        self.assertEqual(response.status_code,200)

class TestAuthenticateUser(TestCase):
    def setUp(self):
        User = get_user_model()
        user = User.objects.create_user('temporary', 'temporary@gmail.com', 'temporary')

    def test_secure_page(self):
        User = get_user_model()
        self.client.login(username='temporary', password='temporary')
        response = self.client.get('/', follow=True)
        user = User.objects.get(username='temporary')
        self.assertTrue(user.is_authenticated)


# class TestModelOfQuery(TestCase):
#     def test_news_and_reports(self):
#         news = News.objects.all()
#         reports = Report.objects.all()
#         self.assertTrue(len(news) != 0 or len(reports) != 0)

    # def test_using_update_status_template(self):
    #     response = Client().get('/donation/')
    #     self.assertTemplateUsed(response,'donation.html')

    # def test_landing_page_is_completed(self):
    #     request = HttpRequest()
    #     response = index(request)
    #     html_response = response.content.decode('utf8')
    #     self.assertIn('Form Donasi', html_response)

#
# class TestModelOfQuery(TestCase):
#     def test_news_and_reports(self):
#         news = News.objects.all()
#         reports = Report.objects.all()

    # def test_using_update_status_template(self):
    #     response = Client().get('/donation/')
    #     self.assertTemplateUsed(response,'donation.html')

    # def test_landing_page_is_completed(self):
    #     request = HttpRequest()
    #     response = index(request)
    #     html_response = response.content.decode('utf8')
    #     self.assertIn('Form Donasi', html_response)
