from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import *
from django.db import IntegrityError
from .forms import ProfileForm
from django.contrib.auth import authenticate
from django.test import Client
from django.contrib.auth import get_user_model


# Create your tests here.
class TestAccountViewsTemplateTest(TestCase):
    def test_get_request(self):
        response = Client().get('/account/login/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'register.html')

    # def test_can_create_account(self):
    #     form_data = {'full_name': 'full name',
    #                 "password" : "password",
    #                 "email" : "email@email.com",
    #                 "birth_date": "2016-10-10"}
    #
    #     response = Client().post('/account/', form_data)
    #     self.assertEqual(response.status_code, 302)

class TestModelProfile(TestCase):
    def test_if_instance_created(self):
        count_before = Profile.objects.count()
        test_profile = Profile.objects.create(full_name="Test Fullname",
                                             birth_date="2016-10-10",
                                             email="hello@hello.com",
                                             password="testpassword")

        self.assertEqual(Profile.objects.count(), count_before + 1)

    def test_email_models_uniqueness(self):
        Profile.objects.create(full_name="Test Fullname",
                                             birth_date="2016-10-10",
                                             email="hello@hello.com",
                                             password="testpassword")


        with self.assertRaises(IntegrityError):
            Profile.objects.create(full_name="Test Fullname",
                                                birth_date="2016-10-10",
                                                email="hello@hello.com",
                                                password="testpassword")

    def test_model_repr_and_str(self):
        user = Profile.objects.create(full_name="Test Fullname",
                                    birth_date="2016-10-10",
                                    email="hello@hello.com",
                                    password="testpassword")

        self.assertEqual(str(user), user.full_name)


class TestFormUser(TestCase):
    def test_forms(self):
        count_before = Profile.objects.count()
        form_data = {'full_name': 'full name',
                    "password" : "password",
                    "email" : "email@email.com",
                    "birth_date": "2016-10-10"}

        form = ProfileForm(data=form_data)
        user = form.save()
        self.assertTrue(form.is_valid())
        self.assertEqual(Profile.objects.count(), count_before + 1)

class TestAuthenticateUser(TestCase):
    def setUp(self):
        User = get_user_model()
        user = User.objects.create_user('temporary', 'temporary@gmail.com', 'temporary')

    def test_secure_page(self):
        User = get_user_model()
        self.client.login(username='temporary', password='temporary')
        response = self.client.get('/', follow=True)
        user = User.objects.get(username='temporary')
        self.assertTrue(user.is_authenticated)
        response = self.client.get('/account/logout/')
        self.assertTrue(response.status_code, 302)


# class TestAccount(TestCase):
#     def test_login_authenticate(self):
#         user = authenticate(username="admin", password="admin")
#         print(user)
#         self.assertEqual(True, user is not None)
