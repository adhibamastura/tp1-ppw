# Create your views here.
from django.shortcuts import render, HttpResponse, redirect, reverse, HttpResponseRedirect
from django.contrib.auth import login as auth_login, authenticate
from datetime import datetime, date
from django.contrib.auth.models import User
from .forms import *
from .models import *

def login(request):
    return render(request, 'register.html')

def logout(request):
    request.session.flush()
    response = HttpResponseRedirect("/")
    response.delete_cookie("sessionid")
    return response

#     request.session.flush()
#     response = HttpResponseRedirect("/")
#     response.delete_cookie("sessionid")
#     print("Hello")
#     return response


# def register(request):
#     response = {}
#     form = CustomUserForm(request.POST or None)
#     if(request.method == "POST"):
#         if form.is_valid():
#             user = form.save()
#             # user =  CustomUser.objects.get(username=request.POST['username'])
#             auth_login(request, user)
#             return redirect('/')

#     response['form'] = form
#     return render(request, 'registration/register.html', response)

# def login(request):
#     if(request.method == "POST"):
#             user = CustomUser.objects.get(username=request.POST['username'])
#             auth_login(request, user)
#             return redirect('/')
#             # user =  CustomUser.objects.get(username=request.POST['username'])
#     return render(request, 'registration/login.html', {})
