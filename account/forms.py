from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
# from .models import Profile
from .models import *


class ProfileForm(forms.ModelForm):

    password = forms.CharField(min_length=3, max_length=200, widget=forms.PasswordInput)
    birth_date = forms.DateField(widget=forms.DateInput(attrs={'type': "date"}))

    class Meta:
        model = Profile
        fields = ('full_name', 'email', 'password', 'birth_date', )
   
    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'


## gajadi pake custom user
## class CustomUserForm(forms.ModelForm):
#     class Meta:
#         model = CustomUser
#         fields = ('username', 'full_name', 'password' ,'email', 'birth_date', )
