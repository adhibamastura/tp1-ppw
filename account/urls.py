from django.contrib import admin
from django.urls import path
from .views import *

app_name = "account"

urlpatterns = [
    # path('', index, name='index'),
    path('login/', login, name='login'),
    path('logout/', logout, name='logout'),
]
