from django.db import models
# from django.contrib.auth.models import AbstractUser
# from django.db.models.signals import post_save
# from django.dispatch import receiver

class Profile(models.Model):
    full_name = models.CharField(max_length=200)
    email = models.EmailField(unique=True)
    ## why charfield? to avoid conflict datepicker
    birth_date = models.CharField(null=True, max_length=200)
    password = models.CharField(max_length=200)

    def __str__(self):
        return self.full_name

## ga jadi pake login

# class CustomUser(AbstractUser):
#     full_name = models.CharField(max_length=200)
#     email = models.EmailField(unique=True)
#     birth_date = models.DateField(null=True)

#     def __str__(self):
#         return self.username
