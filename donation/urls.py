from django.conf.urls import url
from django.urls import path
from .views import *

urlpatterns = [
	path('', index, name = 'donate-index'),
	path('donate/', donate, name = 'donate'),
	path('terimakasih/', terimakasih, name = 'terimakasih'),
]
