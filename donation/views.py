from django.shortcuts import render, redirect
from django.shortcuts import render, HttpResponse, redirect, reverse
from django.http import JsonResponse
from django.urls import reverse
from .forms import DonationForm
from account.forms import ProfileForm
from .models import Donation
from news.models import News
from account.views import *
from account.models import Profile

# Create your views here.
def index(request):
	response = {}
	try:
		form = DonationForm(request.POST or None,  initial=
						{"donatur_email" : request.user.email,
						"donatur_fullname" : request.user.first_name + request.user.last_name})
	except:
		form = DonationForm(request.POST or None)
	return render(request, 'donation.html', {'form': form})

def donate(request):
	form = DonationForm(request.POST or None)
	if(request.method == "POST"):
		if(not request.user.is_authenticated):
			return JsonResponse({"message" : "You haven't logged in, please login", "status_code": 403})
		if(form.is_valid):
			print(request.POST)
			program_id = request.POST['program']
			program = News.objects.get(pk=program_id)
			total_donation = request.POST['total_donation']
			is_anon = True if str(request.POST['is_anon']) == "true" else False
			Donation.objects.create(program=program,
								donatur_fullname=request.user.first_name + request.user.last_name,
								total_donation=total_donation,
								donatur_email=request.user.email,
								is_anon=is_anon)
			return JsonResponse({"message" : "Success", "status_code": 200})
		return JsonResponse({"message" : "Data is not valid", "status_code":403})
	else:
		return JsonResponse({"message" : "/GET method is not allowed"})


def terimakasih(request):
	return render(request,'terimakasih.html')
