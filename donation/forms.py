from django import forms
from news.models import News
from .models import Donation

class DonationForm(forms.ModelForm):
    program = forms.ModelChoiceField(queryset=News.objects.all())
    is_anon = forms.BooleanField(initial=False, required=False)

    class Meta:
        model = Donation
        fields = ('program', 'donatur_fullname', 'donatur_email', 'total_donation', 'is_anon')
        labels = {
            "is_anon": "Ingin berdonasi secara Anonymous?",
        }

    def __init__(self, *args, **kwargs):
        super(DonationForm, self).__init__(*args, **kwargs)
        self.fields['total_donation'].widget.attrs['min'] = 1000
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
        self.fields['is_anon'].widget.attrs['class'] = ""
