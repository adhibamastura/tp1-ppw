from django.db import models

class Donation(models.Model):
    program = models.ForeignKey("news.News", on_delete=models.CASCADE)
    donatur_fullname = models.CharField(max_length=100)
    donatur_email = models.CharField(max_length=100)
    total_donation = models.FloatField()
    is_anon = models.BooleanField()
