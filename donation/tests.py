from django.test import TestCase
from .models import Donation
from account.models import Profile
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from news.models import News
from .views import index
from django.contrib.auth import get_user_model
# Create your tests here.


class TestDonationViewsTemplateTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/donation/')
        self.assertEqual(response.status_code,200)

    def test_get_not_alloed(self):
        response = Client().get('/donation/donate/')
        self.assertEqual(response.status_code,200)

    def test_post_not_authenticated(self):
        response = Client().post('/donation/donate/')
        self.assertEqual(response.status_code,200)



    def test_using_update_status_template(self):
        response = Client().get('/donation/')
        self.assertTemplateUsed(response,'donation.html')

    def test_using_update_status_template(self):
        response = Client().get('/donation/terimakasih/')
        self.assertTemplateUsed(response,'terimakasih.html')

    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Form Donasi', html_response)

class TestDonationModels(TestCase):
    def test_if_instance_created(self):
        before_count = Donation.objects.count()
        user = Profile.objects.create(full_name="test",
                                email="test@haha.com",
                                password="asdqwe123",
                                birth_date="2018-2-13")

        program = News.objects.create(title="test",
                                    image="",
                                    desc="abcd")

        Donation.objects.create(program=program,
                                donatur_email=user.email,
                                donatur_fullname=user.full_name,
                                total_donation=23123123,
                                is_anon=True)
        self.assertEqual(Donation.objects.count(), before_count + 1)


class TestAuthenticateUser(TestCase):
    def setUp(self):
        User = get_user_model()
        user = User.objects.create_user('temporary', 'temporary@gmail.com', 'temporary')

    def test_secure_page(self):
        User = get_user_model()
        self.client.login(username='temporary', password='temporary')
        response = self.client.get('/', follow=True)
        user = User.objects.get(username='temporary')
        self.assertTrue(user.is_authenticated)


    def test_post_with_data(self):
        response = Client().post('/donation/donate/', data={"program" : 1,
                                                            "total_donation" : 1000000,
                                                            "is_anon": "true" })
        self.assertEqual(response.status_code,200)
