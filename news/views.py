from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.db.models import Q

from .models import News
from donation.models import Donation
# Create your views here.

def retrieve(request, id):
	news = News.objects.get(id=id)
	donations = Donation.objects.filter(program=news)
	total = 0
	for donation in donations:
		total += donation.total_donation
	total = str(total)
	response = {
		"total_donation" : total,
		"news" : news,
		"donations" : donations,
	}

	return render(request, 'news-retrieve.html', response)

def index(request):
	news = News.objects.all()
	return render(request, 'news-index.html', {"news" : news})

# def search(request):
# 	return render(request, 'news-index.html', {"news" : news, "search": True})