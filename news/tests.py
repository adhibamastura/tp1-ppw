from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import News

# Create your tests here.
class TestNewsViewsTemplateTest(TestCase):

    def test_retrieve_page(self):
        news = News.objects.create(title="title",
                            image="test",
                            desc="test")

        response = Client().get('/news/' + str(news.id)+"/")
        self.assertEqual(response.status_code,200)

    def test_retrieve_page_not_found(self):
        news = News.objects.create(title="title",
                            image="test",
                            desc="test")

        max_obj = News.objects.count()

        with self.assertRaises(News.DoesNotExist):
            response = Client().get('/news/' + str(max_obj + 1)+"/")

    def test_url_is_exist(self):
        response = Client().get('/news/')
        self.assertEqual(response.status_code,200)

    def test_using_update_status_template(self):
        response = Client().get('/news/')
        self.assertTemplateUsed(response,'news-index.html')

    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Mari Berdonasi!', html_response)

class NewsModelTest(TestCase):
    def test_if_instance_is_created(self):
        before_count = News.objects.count()
        News.objects.create(title="title",
                            image="test",
                            desc="test")
        self.assertEqual(News.objects.count(), before_count + 1)

    