from django.urls import path

from .views import *
urlpatterns = [
	    # path('?search*/', search, name='news-search'),
	    path('', index, name='news-index'),
	    path('<int:id>/', retrieve, name='news-retrieve'),
]