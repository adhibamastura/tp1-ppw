from django.db import models

# Create your models here.
class News(models.Model):
	title = models.CharField(max_length=200)
	image = models.ImageField(upload_to="static/img")
	desc = models.TextField()

	def __str__(self):
		return self.title

	def __repr__(self):
		return self.title