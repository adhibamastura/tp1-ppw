from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import Report


# Create your tests here.
class HomeViewsTemplateTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code,200)

    def test_using_template_report_index(self):
        response = Client().get('/home/')
        self.assertTemplateUsed(response,'report-index.html')

    def test_using_template_report_retrieve(self):
        response = Client().get('/home/1/')
        self.assertEqual(response.status_code,302)
        Report.objects.create(title="a", desc="a")
        response = Client().get('/home/1/')
        self.assertTemplateUsed(response,'report-retrieve.html')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Berita', html_response)


# Create your tests here.
class ReportModelTest(TestCase):
    def test_if_instance_is_created(self):
        before_count = Report.objects.count()
        Report.objects.create(title="title",
                            image="test",
                            desc="test")
        self.assertEqual(Report.objects.count(), before_count + 1)
