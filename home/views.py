from django.shortcuts import render, HttpResponse
from django.http import HttpResponseRedirect
from .models import Report

def retrieve(request, id):
	try:
		report = Report.objects.get(id=id)
	except:
		return HttpResponseRedirect("/")
	return render(request, 'report-retrieve.html', {"report" : report})

def index(request):
	reports = Report.objects.all()
	return render(request, 'report-index.html', {"reports" : reports})
