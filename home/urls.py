from django.contrib import admin
from django.urls import path, include
from .views import *

urlpatterns = [
    path('', index, name='home-index'),
    path('<int:id>/', retrieve, name='report-retrieve')
]