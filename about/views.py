from django.shortcuts import render
import json
from .models import Testimoni
from django.core import serializers
from django.http import HttpResponseRedirect,JsonResponse,HttpResponse
import datetime
from .forms import TestimoniForm
from django.utils.html import strip_tags


# Create your views here.
def index(request):
	return render(request,'testimoni.html')

def jsonAbout(request):
	form = TestimoniForm(request.POST or None)
	if request.method == "POST":
		if(form.is_valid):
			waktu = datetime.datetime.now().strftime('%a, %d %b %Y / %I:%M %p')
			usernama = request.user.username
			testimoni = strip_tags(request.POST['comment'])
			testimoni_model = Testimoni(username=usernama, comment = testimoni, time=waktu)
			testimoni_model.save()
			return render(request,'testimoni.html')
		return render(request, 'testimoni.html')
	return JsonResponse({"data" : "not allowed", "status_code":"403"})

def allComment(request):
	allComment = Testimoni.objects.all().values('username', 'comment','time').order_by("-id")
	allComment2 = list(allComment)
	return JsonResponse(allComment2, safe=False)
