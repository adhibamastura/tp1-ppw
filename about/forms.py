
from django.forms import ModelForm
from .models import Testimoni


class TestimoniForm(ModelForm):
    class Meta:
        model = Testimoni
        fields = '__all__'
