from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest
from django.contrib.auth import get_user_model
# Create your tests here.
class about(TestCase):
	def test_about_url_is_exist(self):
		response = Client().get('/about/')
		self.assertEqual(response.status_code, 200)

	def test_allcomments_url_is_exist(self):
		response = Client().get('/about/allComment/')
		self.assertEqual(response.status_code, 200)

	def test_about_using_index_func(self):
		found = resolve('/about/')
		self.assertEqual(found.func, index)

	def test_nama_template(self):
		response = Client().get('/about/')
		self.assertTemplateUsed(response, 'testimoni.html')

	def test_text(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Tentang Kami', html_response)

class TestAuthenticateUser(TestCase):
    def setUp(self):
        User = get_user_model()
        user = User.objects.create_user('temporary', 'temporary@gmail.com', 'temporary')

    def test_secure_page(self):
        User = get_user_model()
        self.client.login(username='temporary', password='temporary')
        response = self.client.get('/', follow=True)
        user = User.objects.get(username='temporary')
        self.assertTrue(user.is_authenticated)

    def test_post_with_data(self):
        response = Client().post('/about/jsonAbout/', data={"comment" : "adasdasd"})
        self.assertEqual(response.status_code,200)
