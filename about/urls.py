from django.conf.urls import url
from django.urls import path
from .views import index
from .views import jsonAbout
from .views import allComment

urlpatterns = [
	path('', index, name = 'testimoni'),
	path('jsonAbout/', jsonAbout, name = 'jsonAbout'),
	path('allComment/', allComment, name = 'allComment'),
]