from django.db import models
import datetime as dt
from datetime import datetime

# Create your models here.
class Testimoni(models.Model):
    username = models.CharField(max_length=100)
    comment = models.CharField(max_length=200)
    time = models.CharField(max_length=200, default=datetime.now())
