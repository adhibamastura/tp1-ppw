from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest

# Create your tests here.

class tp2UnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/donatur/')
        self.assertEqual(response.status_code, 200)

    # def test_using_donatur_template(self):
    #     response = Client().get('/donatur/')
    #     self.assertTemplateUsed(response,'donatur.html')

    def test_donatur_template(self):
        response = Client().get('/donatur/')
        html_response = response.content.decode('utf8')
        self.assertIn('List Program yang Pernah di Donasi', html_response)

    # def test_using_donatur_function(self):
    #     found = resolve('/donatur/')
    #     self.assertEqual(found.func, donatur)
