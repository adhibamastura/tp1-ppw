from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from django.contrib.auth import authenticate
from donation.models import Donation

# Create your views here.

# def donatur(request):
#     response = {}
#     return render(request, 'donatur.html', response)

def donatur(request):
    response = {}
    email = request.user.username + '@gmail.com'
    # try:
    # 	donatur = Donatur.objects.get(email=email)
    # except Exception as e:
    # response['message'] = 'Email Not Found!'
    listDonations = Donation.objects.filter(donatur_email=email)
    total_donation = 0
    for donation in listDonations:
        total_donation += donation.total_donation

    response ['listDonations'] = listDonations
    response ['total_donation'] = total_donation

    return render(request, 'donatur.html', response)
