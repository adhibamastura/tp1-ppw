# Tugas Pemrograman 1: PPW Kelompok 7 Kelas D

[Heroku Link](http://ppw-d7-tugas1.herokuapp.com/)

## Status
[![pipeline status](https://gitlab.com/adhibamastura/tp1-ppw/badges/master/pipeline.svg)](https://gitlab.com/adhibamastura/tp1-ppw/commits/master) [![coverage report](https://gitlab.com/adhibamastura/tp1-ppw/badges/master/coverage.svg)](https://gitlab.com/adhibamastura/tp1-ppw/commits/master)

## List Mahasiswa

**Name**  Muhammad Fachry Nataprawira  
**NPM:** 1706039704   
**Gitlab:** [ciferivalle](http://gitlab.com/ciferivalle)  

**Name** Adhiba Mastura  
**NPM:** 1706074764   
**Gitlab:** [adhibamastura](https://gitlab.com/adhibamastura)  

**Name** Felia Risky Faizal  
**NPM:** 1706984581   
**Gitlab:** [feliarf](https://gitlab.com/feliarf)  

**Name** Hana Dior  
**NPM:** 1706025182   
**Gitlab:** [hanadior](https://gitlab.com/hanadior)  

